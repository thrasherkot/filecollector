package client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class FileClient extends Thread{

	public static final Logger log = Logger.getLogger(FileClient.class.getName());

	private String serverIp = "192.168.1.101";
	DataInputStream in;
	private DataOutputStream out;
	private Socket socket;
	private boolean stopped = false;
	public final Object lock = new Object();

	public static void main(String[] args) {
		startLog();
		new FileClient().start();
	}

	private static void startLog(){
		try {
			LogManager.getLogManager().readConfiguration(FileClient.class.getResourceAsStream("/logging.properties"));
			FileHandler handler = new FileHandler(FileClient.class.getName() + "Log%u%g.txt", true);
			log.addHandler(handler);
			ConsoleHandler consoleHandler = new ConsoleHandler();
			log.addHandler(consoleHandler);
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void run(){
		try{
			init();

			InetAddress ipAddress = InetAddress.getByName(serverIp);
			int serverPort = 6678;
			socket = new Socket(ipAddress, serverPort);
			log.log(Level.INFO, "Connection established to " + serverIp + "\n");
			in = new DataInputStream(socket.getInputStream());
			out = new DataOutputStream(socket.getOutputStream());
			BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
			ServerAnswerListener answerListener = new ServerAnswerListener(this);
			answerListener.setDaemon(true);
			answerListener.start();
			String command;
			System.err.println("Type a command, or type help to see the commands list. \n");
			while (!stopped) {
				System.err.print(">");
				synchronized (lock) {
					command = keyboard.readLine();
					out.writeUTF(command);
					out.flush();
					lock.wait();
				}
			}
		} catch(SocketException e){
			if (e.toString().contains("Connection reset by peer")){
				log.log(Level.WARNING, "Connection lost. Server is down." + "\n");
			} else {
				log.log(Level.SEVERE, "Error during connection to " + serverIp + " " + e);
			}
		} catch (IOException e){
			log.log(Level.SEVERE, "Error while reading command " + e + "\n");
		} catch (InterruptedException e){
			log.log(Level.SEVERE, "Error while waiting for command processing " + e + "\n");
		}
	}
	
	public void saveFile(String fileName){
		try{
			File file = new File(System.getProperty("user.dir") + System.getProperty("file.separator") + fileName);
			
			if (!file.exists()) {
				file.createNewFile();
			}
			
			FileOutputStream fos = new FileOutputStream(file, false);
			synchronized (in) {
				long fileSize = in.readLong();
				byte[] byteArray = new byte[8192];
				long rounds = fileSize / byteArray.length;
				int tail = (int) (fileSize % byteArray.length);
				for (int i = 0; i < rounds; i++) {
					in.readFully(byteArray);
					fos.write(byteArray);
				}
				in.readFully(byteArray, 0, tail);
				fos.write(byteArray, 0, tail);
				fos.close();
				log.log(Level.INFO, "File downloaded " + fileName + "\n");
			}
		}
		catch (IOException e){
			log.log(Level.SEVERE, "Error during file saving " + e.toString() + "\n");
		}
	}
	
	private void init() throws IOException{
		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		System.err.println("Enter the server's IP:");
		String ipRegExp = "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
				+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
		Pattern pattern = Pattern.compile(ipRegExp);
		while (true){
		String ip = keyboard.readLine();
		Matcher matcher = pattern.matcher(ip);
			if (!matcher.matches()){
				System.err.println("Invalid IP address. Please write the correct IP address");
			} else {
				serverIp = ip;
				break;
			}
		}
	}

	public void close() {
		try {
			synchronized (in) {
				in.close();
			}
			stopped = true;
			socket.close();
			out.close();
			log.log(Level.INFO, "Connection closed " + serverIp + "\n");
		} catch (IOException e) {
			log.log(Level.SEVERE, "Error during closing connection " + e + "\n");
			e.printStackTrace();
		}
	}
}
