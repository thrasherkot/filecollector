package client;


import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

class ServerAnswerListener extends Thread{

    private FileClient client;
    private Logger log = FileClient.log;
    private boolean stop = false;

    public ServerAnswerListener(FileClient client){
        this.client = client;
    }

    @Override
    public void run(){
        String serverAnswer;
        try {
            while (!stop) {
                synchronized (client.in) {
                    serverAnswer = client.in.readUTF();
                }
                analyzeAnswer(serverAnswer);
            }
        } catch (IOException e){
            log.log(Level.SEVERE, "Error during receiving message from sever: " + e.toString());
        }
    }

    private void analyzeAnswer(String serverAnswer) {
        synchronized (client.lock) {
            client.lock.notify();
            if (serverAnswer.equals("exit")) {
                stop = true;
                client.close();
            }
            if (serverAnswer.startsWith("sending ")) {
                String fileName = serverAnswer.replaceFirst("sending ", "");
                System.err.println("receiving file " + fileName);
                client.saveFile(fileName);
            } else System.err.println(serverAnswer + "\n");
        }
    }


}



