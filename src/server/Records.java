package server;

class Records {
	
	private String name;
	private int counter;
	
	public Records(String name){
		this.name = name;
		this.counter = 0;
	}
	
	public void increaseCounter(){
		counter++;
	}
	
	@Override
	public String toString(){
		return String.format(StatisticSaver.format, name, counter);
	}

}
