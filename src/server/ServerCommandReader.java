package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

class ServerCommandReader extends Thread{

    private FileServer server;
    private Logger log = FileServer.log;

    public ServerCommandReader (FileServer server){
        this.server = server;
    }

    @Override
    public void run() {
        try {
            while (true) {
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                String command = in.readLine();
                if (command.equals("exit")){
                    System.err.println("Preparing to shutting down.");
                    server.closeServer();
                    in.close();
                    break;
                }
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, "Error while reading command from server command line " + e + "\n");
        }
    }
}
