package server;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.logging.*;

class FileServer {

	public List<Connection> connections =
			Collections.synchronizedList(new ArrayList<Connection>());
	public List<String> sendingNow =
			Collections.synchronizedList(new ArrayList<String>());
	public String fileDir = "";
	public StatisticSaver statistic;
	private boolean stop = false;
	public static final Logger log = Logger.getLogger(FileServer.class.getName());
	private ServerSocket server;
	private FileWorker worker;

	
	private FileServer() {
		choosingFolder();
		serverStart();
		loop();
	}

	private void choosingFolder(){
		try {
			String systemDelimiter = System.getProperty("file.separator");
			System.err.println("Please enter path to directory with files:");
			while (true) {
				BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
				String command = in.readLine();
				File dir = new File(command);
				if (dir.exists()) {
					fileDir = dir.getCanonicalPath();
					break;
				} else {
					System.err.println("Directory doesn't exist. Please try again.");
				}
			}
			if (fileDir.charAt(fileDir.length() - 1) != systemDelimiter.charAt(0)) {
				fileDir = fileDir + systemDelimiter;
			}
		} catch (IOException e) {
			log.log(Level.SEVERE, "Error in file dir choosing " + e.toString() + "\n");
		}
	}

	private void serverStart(){
		try {
			server = new ServerSocket(6678);
			server.setSoTimeout(3000);
			log.log(Level.INFO, "Server started at " + 6678 + "\n");
			log.log(Level.INFO, "Server's file directory: " + fileDir + "\n");
			log.log(Level.INFO, "To safety closing server type exit.");
			ServerCommandReader commandReader = new ServerCommandReader(this);
			commandReader.setDaemon(true);
			commandReader.start();
			worker = new FileWorker(fileDir);
			statistic = new StatisticSaver(this, worker);
			Timer timer = new Timer(true);
			long period = 10000;
			timer.schedule(statistic, period, period);
		} catch (IOException e) {
			log.log(Level.SEVERE, "Error in server starting " + e + "\n");
			e.printStackTrace();
		}
	}

	private void loop(){
		try {
			while (!stop) {
				try {
					Socket socket = server.accept();
					socket.setSoTimeout(3000);
					Connection con = new Connection(socket, this, worker);
					connections.add(con);
					con.start();
				} catch (SocketTimeoutException ignored){
					// this exception throw by server.accept action. It need to avoid endless waiting for new connection when we trying to shutting down the server
					// so there we do nothing. If we catch this exception and stop = false, then server will try accept another connection, else it will end of the loop and server will stop.
				}
			}
		}
		catch (IOException e) {
			log.log(Level.SEVERE, "Error while server running " + e + "\n");
		}
	}

	public boolean isStopped(){
		return stop;
	}

	private boolean isFilesSendNow(){
		synchronized(sendingNow){
			return sendingNow.size()>0;
		}
	}

	public void closeServer(){
		stop = true;
//		check for finishing all file sending process before server will turned off
		while(isFilesSendNow()){
			log.log(Level.INFO, "waiting until file sending end.");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				log.log(Level.SEVERE, "Error in waiting for saving file " + e.toString() + "\n");
				e.printStackTrace();
			}
		}
		while (connections.size()>0) {
			connections.get(0).close();
		}
		log.log(Level.INFO, "Server shutting down. \n ");
	}

	private static void startLog(){
		try {
			LogManager.getLogManager().readConfiguration(FileServer.class.getResourceAsStream("/logging.properties"));
			FileHandler handler = new FileHandler(FileServer.class.getName() + "Log%u%g.txt", true);
			log.addHandler(handler);
			ConsoleHandler consoleHandler = new ConsoleHandler();
			log.addHandler(consoleHandler);

		} catch (SecurityException | IOException e) {
			log.log(Level.SEVERE, "Error while server running ", e);
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		startLog();
		new FileServer();
	}

}
