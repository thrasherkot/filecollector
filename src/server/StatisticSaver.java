package server;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StatisticSaver extends TimerTask {

    public static String format = "%-50s%-10s%n";
    public static int maxNameLength = 10;
    private FileWriter writer;
    private FileServer server;
    private Logger log = FileServer.log;
    private  FileWorker fileWorker;

    public StatisticSaver(FileServer server, FileWorker fileWorker){
        this.server = server;
        this.fileWorker = fileWorker;
    }

    @Override
    public void run(){
        File stats = new File(System.getProperty("user.dir") + System.getProperty("file.separator") + "statistic.txt");
        try{
            if (!stats.exists()){
                stats.createNewFile();
            }
            writer = new FileWriter(stats,false);
            writeStats();
            writer.close();
        }
        catch (IOException e){
            if (e.getClass().toString().contains("FileNotFoundException")){
                log.log(Level.SEVERE, "Unexpected directory behavior. " + e.toString() + "\n");
                server.closeServer();
            } else {
                log.log(Level.SEVERE, "Error in creating statistic's file " + e.toString() + "\n");
            }
        }
    }

    private void writeStats() {
        format = setFormat();
        String str = String.format(format, "FileName","Downloads");
        try{
            writer.write(str);
            for (Records rec: fileWorker.getFilesList()){
                writer.flush();
                writer.write(rec.toString());
            }
        } catch(IOException e){
            log.log(Level.SEVERE, "Error in saving statistic " + e.toString() + "\n");
        }
    }

    private String setFormat() {
        return "%-" + (maxNameLength +1) + "s%-10s%n";
    }
}
