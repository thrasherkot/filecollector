package server;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

class FileWorker {
	
	private final String directory;
	private List <Records> statistic = new ArrayList<>();
	private List <String> files = new ArrayList<>();
	
	public FileWorker (String dir) throws FileNotFoundException{
		this.directory = dir;
		scanDirectory();
	}
	
	public String scanDirectory() throws FileNotFoundException{
		File folder = new File(directory);
		if (folder.exists()){
			File[] listOfFiles = folder.listFiles();
			if (listOfFiles == null) {
				return "Folder is empty. Nothing to download";
			}
			StringBuilder sb = new StringBuilder();
			int maxLength = StatisticSaver.maxNameLength;
			for (File file : listOfFiles) {
				if (file.isFile()) {
					String name = file.getName();
					if (!files.contains(name)) {
						statistic.add(new Records(name));
						files.add(name);
						if (name.length() > maxLength) maxLength = name.length();
					}
					sb.append(file.getName()).append(" \n");
				}
			}
			StatisticSaver.maxNameLength = maxLength;
			return sb.toString();
		} else {
			throw new FileNotFoundException("Can't find the directory. May be it was deleted or moved. " + directory);
		}
	}
	
	public List<Records> getFilesList() throws FileNotFoundException{
		scanDirectory();
		return statistic;
	}

	public boolean fileExist(String fileName) throws FileNotFoundException {
		scanDirectory();
		File file = new File(directory + fileName);
		return file.exists();
	}
	
	public void incDownloadCounterForFile(String fileName){
		int position = files.indexOf(fileName);
		statistic.get(position).increaseCounter();
	}

}
