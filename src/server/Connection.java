package server;

import java.io.*;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

class Connection extends Thread {

    private DataInputStream in;
    private DataOutputStream out;
    private Socket socket;
    private boolean stopConnect = false;
    private FileServer server;
    private Logger log = FileServer.log;
    private FileWorker fileWorker;

    public Connection(Socket socket,FileServer server, FileWorker fileWorker) {
        this.socket = socket;
        this.server = server;
        this.fileWorker = fileWorker;

        try {
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            log.log(Level.SEVERE, "Error in getting input/output stream from socket " + e.toString() + "\n");
            close();
        }
    }

    @Override
    public void run() {
        try {
            log.log(Level.INFO, "Client connected from " + socket.getInetAddress().toString() + "\n");
            String command;
            while (!stopConnect) {
                try {
                    command = in.readUTF();
                    commandChecker(command);
                } catch (SocketTimeoutException ignored){

                }
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, "Error in receiving command from client " + e + "\n");
        }
    }

    private void commandChecker(String command){
        try{
            if (server.isStopped()){
                out.writeUTF("Server goes down. Closing connection");
                stopConnect = true;
                close();
            }
            if (command.startsWith("get ")){
                String fileName = command.replaceFirst("get ", "");
                sendFile(fileName);
            } else {
                switch (command){

                    case "exit": close();
                        break;

                    case "help" :  out.writeUTF("Command list: \n help - show this manual "
                           + "\n list - show files list \n get <filename> - download file with name \"filename\"");
                        break;

                    case "list": out.writeUTF(fileWorker.scanDirectory());
                        break;

                    default: out.writeUTF("wrong command. please use \"help\"");
                }
            }
            out.flush();
        } catch (IOException e) {
            log.log(Level.SEVERE, "Error in sending command to client " + e.toString() + "\n");
        }
    }

    private void sendFile(String fileName){
        if (System.getProperty("os.name").contains("Windows"))
            try {
                fileName = new String(fileName.getBytes(), "cp866");
            } catch (UnsupportedEncodingException e){
                log.log(Level.WARNING, "Error in string formatting " + e.toString());
                e.printStackTrace();
            }
        try {
            if (fileWorker.fileExist(fileName)){
                File file = new File (server.fileDir + fileName);
                if (file.isFile()){
                    out.writeUTF("sending " + fileName);
                    String s = socket.toString();
                    server.sendingNow.add(s);
                    log.log(Level.INFO, "File sending started: " + fileName + " sending to " + socket.getInetAddress().toString() + "\n");
                    out.flush();

                    long fileSize = file.length();
                    out.writeLong(fileSize);
                    FileInputStream fis = new FileInputStream(file);
                    byte[] byteArray = new byte[8192];
                    int read;
                    while ((read = fis.read(byteArray)) > 0){
                        out.write(byteArray,0,read);
                    }
                    fis.close();
                    out.flush();
                    log.log(Level.INFO, "File " + fileName + " sended to " + socket.getInetAddress().toString() + "\n");
                    fileWorker.incDownloadCounterForFile(fileName);
                    server.statistic.run();
                    server.sendingNow.remove(s);
                } else {
                    out.writeUTF("You can't copy directory. Please type filename.");
                }
            } else {
                out.writeUTF("file doesn't exist. try to choose a correct filename");
            }
        }
        catch (IOException e){
           log.log(Level.SEVERE, "Error in file sending process " + e.toString() + "\n");
        }
    }

    public void close() {
        String address = socket.getInetAddress().toString();
        try {
            stopConnect = true;
            out.writeUTF("exit");
            server.connections.remove(this);
            log.log(Level.INFO, "Close client connection " + address + "\n");
        } catch (IOException e){
            log.log(Level.SEVERE, "Error in closing connection with " + address + " " + e + "\n");
        }
    }
}